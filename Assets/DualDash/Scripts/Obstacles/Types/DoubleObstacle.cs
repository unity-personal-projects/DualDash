﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DualDash.Events;

public class DoubleObstacle : Obstacle
{
    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        CharacterPosition charPos = PathRandomPosition(0, 2);
        transform.position = base.SetPosition(transform.position, charPos);
    }
}
