﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DualDash.Events;

public class DoorObstacle : Obstacle
{
    [SerializeField] private List<GameObject> m_DoubleObstaclePrefabs = new List<GameObject>();
    [SerializeField] private List<GameObject> m_SimpleObstaclePrefabs = new List<GameObject>();

    private GameObject m_DoubleObstacle = null;
    private List<GameObject> m_SingleObstacles = new List<GameObject>();
    private List<GameObject> m_Doors = new List<GameObject>();

    protected override void Awake()
    {
    }

    private void Start()
    {
        if (Random.Range(0f, 100f) < 30f)
        {
            GenerateSingleObstacle();
        }
        else
        {
            GenerateDoubleObstacle();
        }
    }

    private void GenerateDoubleObstacle()
    {
        m_DoubleObstacle = Instantiate(m_DoubleObstaclePrefabs[Random.Range(0, m_DoubleObstaclePrefabs.Count)]);
        CharacterPosition doubleObstaclePath = PathRandomPosition(0, 1);
        m_DoubleObstacle.transform.SetParent(transform);
        m_DoubleObstacle.transform.localPosition = Vector3.right * (float)doubleObstaclePath;
        m_DoubleObstacle.transform.position = base.SetPosition(m_DoubleObstacle.transform.position, doubleObstaclePath);

        CharacterPosition firstDoorPosition;
        CharacterPosition secondDoorPosition;
        switch (doubleObstaclePath)
        {
            case CharacterPosition.BorderLeft:
                firstDoorPosition = CharacterPosition.Right;
                secondDoorPosition = CharacterPosition.BorderRight;
                break;
            case CharacterPosition.Left:
                firstDoorPosition = CharacterPosition.BorderLeft;
                secondDoorPosition = CharacterPosition.BorderRight;
                break;
            default:
                firstDoorPosition = CharacterPosition.BorderLeft;
                secondDoorPosition = CharacterPosition.Left;
                break;
        }
        m_Doors.Add(Instantiate(m_Models[Random.Range(0, m_Models.Count)]));
        m_Doors[0].transform.SetParent(transform);
        m_Doors[0].transform.localPosition = Vector3.right * (float)firstDoorPosition;
        m_Doors[0].transform.position = base.SetPosition(m_Doors[0].transform.position, firstDoorPosition);

        m_Doors.Add(Instantiate(m_Models[Random.Range(0, m_Models.Count)]));
        m_Doors[1].transform.SetParent(transform);
        m_Doors[1].transform.localPosition = Vector3.right * (float)secondDoorPosition;
        m_Doors[1].transform.position = base.SetPosition(m_Doors[1].transform.position, secondDoorPosition);
    }

    private void GenerateSingleObstacle()
    {
        m_SingleObstacles.Add(Instantiate(m_SimpleObstaclePrefabs[Random.Range(0, m_SimpleObstaclePrefabs.Count)]));
        m_SingleObstacles.Add(Instantiate(m_SimpleObstaclePrefabs[Random.Range(0, m_SimpleObstaclePrefabs.Count)]));

        CharacterPosition simpleObstacle1Path = PathRandomPosition(0, 1);
        CharacterPosition simpleObstacle2Path;
        CharacterPosition door1Path;
        CharacterPosition door2Path;
        switch (simpleObstacle1Path)
        {
            case CharacterPosition.BorderLeft:
                simpleObstacle2Path = CharacterPosition.Right;
                door1Path = CharacterPosition.Left;
                door2Path = CharacterPosition.BorderRight;
                break;
            default:
                simpleObstacle2Path = CharacterPosition.BorderRight;
                door1Path = CharacterPosition.BorderLeft;
                door2Path = CharacterPosition.Right;
                break;
        }

        m_SingleObstacles[0].transform.SetParent(transform);
        m_SingleObstacles[0].transform.localPosition = Vector3.right * (float)simpleObstacle1Path;
        m_SingleObstacles[0].transform.position = base.SetPosition(m_SingleObstacles[0].transform.position, simpleObstacle1Path);

        m_SingleObstacles[1].transform.SetParent(transform);
        m_SingleObstacles[1].transform.localPosition = Vector3.right * (float)simpleObstacle2Path;
        m_SingleObstacles[1].transform.position = base.SetPosition(m_SingleObstacles[1].transform.position, simpleObstacle2Path);

        m_Doors.Add(Instantiate(m_Models[Random.Range(0, m_Models.Count)]));
        m_Doors[0].transform.SetParent(transform);
        m_Doors[0].transform.position = base.SetPosition(m_Doors[0].transform.position, door1Path);

        m_Doors.Add(Instantiate(m_Models[Random.Range(0, m_Models.Count)]));
        m_Doors[1].transform.SetParent(transform);
        m_Doors[1].transform.position = base.SetPosition(m_Doors[1].transform.position, door2Path);
    }
}
