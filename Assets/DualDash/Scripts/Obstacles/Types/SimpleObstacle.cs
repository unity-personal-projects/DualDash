﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DualDash.Events;

public class SimpleObstacle : Obstacle
{
    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        CharacterPosition charPos = PathRandomPosition(0, 3);
        transform.position = base.SetPosition(transform.position, charPos);

        m_ActiveModel.transform.localRotation = Quaternion.AngleAxis(Random.Range(0f, 360f), Vector3.up);
        m_ActiveModel.transform.localRotation *= Quaternion.AngleAxis(Random.Range(-10f, 10f), Vector3.forward);
        m_ActiveModel.transform.localRotation *= Quaternion.AngleAxis(Random.Range(-10f, 10f), Vector3.right);
    }
}
