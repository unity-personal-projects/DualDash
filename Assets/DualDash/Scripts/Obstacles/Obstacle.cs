﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DualDash.Events;

public class Obstacle : MonoBehaviour
{
    [SerializeField]
    protected List<GameObject> m_Models = new List<GameObject>();
    protected GameObject m_ActiveModel = null;

    protected virtual void Awake()
    {
        m_ActiveModel = m_Models[Random.Range(0, m_Models.Count)];
        foreach (GameObject g in m_Models)
        {
            if (g.GetInstanceID() != m_ActiveModel.GetInstanceID())
            {
                Destroy(g);
            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<ObstacleDestructor>())
        {
            EventManager.Instance.Raise(new ObstacleDestructionEvent() { obstacle = transform.parent.gameObject });
        }
    }

    protected Vector3 SetPosition(Vector3 vect, CharacterPosition position)
    {
        RaycastHit hit;
        LayerMask layer = LayerMask.GetMask("World");
        if (Physics.Raycast(vect + Vector3.up * 30f, Vector3.down, out hit, 70f, layer))
        {
            return new Vector3((float)position, hit.point.y, vect.z);
        }
        return new Vector3((float)position, vect.y, vect.z);
    }

    protected virtual CharacterPosition PathRandomPosition(int minimum, int maximum)
    {
        return (CharacterPosition)(Random.Range(minimum, maximum + 1) * 2 - 3);
    }
}
