﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DualDash.Events;

public class ObstacleSelector : MonoBehaviour
{
    [SerializeField] private List<GameObject> m_ObstaclePrefab = new List<GameObject>();

    private void Awake()
    {
        int i = Random.Range(0, m_ObstaclePrefab.Count);
        GameObject g = Instantiate(m_ObstaclePrefab[i]);
        g.transform.SetParent(transform);
    }
}
