﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DualDash.Events;

public enum CharacterPosition
{
    BorderLeft = -3,
    Left = -1,
    Right = 1,
    BorderRight = 3
}

public class PlayerCharacter
{
    public GameObject gameObject = null;
    public CharacterPosition position = CharacterPosition.Left;
    public float initialOffset = 0f;
    public float offset = 0f;
    public CharacterType type = CharacterType.Right;
}

public class CharacterManager : MonoBehaviour
{
    [SerializeField] private float m_TransitionDelay = 0.1f;
    [SerializeField] private float m_DeathDelay = 15f;
    [SerializeField] private Material m_WolfDefaultMaterial = null;
    [SerializeField] private Material m_WolfDeadMaterial = null;
    private PlayerCharacter m_RightCharacter = null;
    private PlayerCharacter m_LeftCharacter = null;

    private void Awake()
    {
        foreach (Character c in GetComponentsInChildren<Character>())
        {
            if (c.Type == CharacterType.Left)
            {
                m_LeftCharacter = new PlayerCharacter()
                {
                    gameObject = c.gameObject,
                    initialOffset = 1 + c.gameObject.transform.position.x,
                    offset = 1 + c.gameObject.transform.position.x,
                    type = c.Type
                };
            }
            else
            {
                m_RightCharacter = new PlayerCharacter()
                {
                    gameObject = c.gameObject,
                    initialOffset = 1 + c.gameObject.transform.position.x,
                    offset = 1 + c.gameObject.transform.position.x,
                    type = c.Type
                };
            }
        }
        EventManager.Instance.AddListener<InputSwipeLeftEvent>(OnInputSwipeLeft);
        EventManager.Instance.AddListener<InputSwipeRightEvent>(OnInputSwipeRight);
        EventManager.Instance.AddListener<InputSwipeUpEvent>(OnInputSwipeUp);
        EventManager.Instance.AddListener<InputSwipeDownEvent>(OnInputSwipeDown);
        EventManager.Instance.AddListener<CharacterDeadEvent>(OnCharacterDead);
    }

    private void OnCharacterDead(CharacterDeadEvent e)
    {
        if (!m_RightCharacter.gameObject.activeSelf || !m_LeftCharacter.gameObject.activeSelf)
        {
            EventManager.Instance.Raise(new PlayerDeadEvent());
            return;
        }
        StartCoroutine(KillWolf(e.character));
    }
    private IEnumerator KillWolf(GameObject character)
    {
        character.SetActive(false);
        m_LeftCharacter.offset = 0f;
        m_RightCharacter.offset = 0f;
        if (character.GetInstanceID() == m_RightCharacter.gameObject.GetInstanceID()) // RIGHT DEAD
        {
            StartCoroutine(centerCharacter(m_LeftCharacter));
            Vector3 endPos = Vector3.right * ((int)m_RightCharacter.position + m_RightCharacter.offset);
            m_RightCharacter.gameObject.transform.localPosition = endPos;
        }
        else // LEFT DEAD
        {
            Vector3 endPos = Vector3.right * ((int)m_LeftCharacter.position + m_LeftCharacter.offset);
            m_LeftCharacter.gameObject.transform.localPosition = endPos;
            StartCoroutine(centerCharacter(m_RightCharacter));
        }
        yield return new WaitForSeconds(m_DeathDelay);
        m_LeftCharacter.gameObject.GetComponent<Collider>().enabled = false;
        m_RightCharacter.gameObject.GetComponent<Collider>().enabled = false;
        
        character.SetActive(true);
        m_LeftCharacter.offset = m_LeftCharacter.initialOffset;
        m_RightCharacter.offset = m_RightCharacter.initialOffset;
        if (character.GetInstanceID() == m_RightCharacter.gameObject.GetInstanceID()) // RIGHT DEAD
        {
            m_RightCharacter.position = m_LeftCharacter.position;
            Vector3 endPos = Vector3.right * ((int)m_RightCharacter.position + m_RightCharacter.offset);
            m_RightCharacter.gameObject.transform.localPosition = endPos;
            StartCoroutine(RealignCharacter(m_LeftCharacter));
        }
        else // LEFT DEAD
        {
            m_LeftCharacter.position = m_RightCharacter.position;
            Vector3 endPos = Vector3.right * ((int)m_LeftCharacter.position + m_LeftCharacter.offset);
            m_LeftCharacter.gameObject.transform.localPosition = endPos;
            StartCoroutine(RealignCharacter(m_RightCharacter));
        }

        // Cooldown death
        m_LeftCharacter.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material = m_WolfDeadMaterial;
        m_RightCharacter.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material = m_WolfDeadMaterial;
        yield return new WaitForSeconds(2f);
        m_LeftCharacter.gameObject.GetComponent<Collider>().enabled = true;
        m_RightCharacter.gameObject.GetComponent<Collider>().enabled = true;
        m_LeftCharacter.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material = m_WolfDefaultMaterial;
        m_RightCharacter.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material = m_WolfDefaultMaterial;
    }

    private void OnInputSwipeDown(InputSwipeDownEvent e)
    {
        if (!m_RightCharacter.gameObject.activeSelf || !m_LeftCharacter.gameObject.activeSelf)
        {
            return;
        }
        int dist = CharacterLinesDistance();
        if (dist > 2)
        {
            m_RightCharacter.offset = 0f;
            m_LeftCharacter.offset = 0f;

            CharacterPosition destPos = GetCharacterDestPos(m_RightCharacter, false);
            StartCoroutine(Swipe(m_RightCharacter, destPos));

            destPos = GetCharacterDestPos(m_LeftCharacter, true);
            StartCoroutine(Swipe(m_LeftCharacter, destPos));
        }
        else if (dist > 1)
        {
            m_RightCharacter.offset = m_RightCharacter.initialOffset;
            m_LeftCharacter.offset = m_LeftCharacter.initialOffset;

            CharacterPosition destPos = GetCharacterDestPos(m_RightCharacter, false);
            StartCoroutine(Swipe(m_RightCharacter, destPos));

            destPos = GetCharacterDestPos(m_LeftCharacter, true);
            StartCoroutine(Swipe(m_LeftCharacter, destPos));
        }
        else if (dist > 0)
        {
            m_RightCharacter.offset = m_RightCharacter.initialOffset;
            m_LeftCharacter.offset = m_LeftCharacter.initialOffset;

            CharacterPosition destPos = GetCharacterDestPos(m_LeftCharacter, true);
            StartCoroutine(Swipe(m_LeftCharacter, destPos));
            StartCoroutine(RealignCharacter(m_RightCharacter));
        }
    }

    private void OnInputSwipeUp(InputSwipeUpEvent e)
    {
        if (!m_RightCharacter.gameObject.activeSelf || !m_LeftCharacter.gameObject.activeSelf)
        {
            return;
        }
        m_RightCharacter.offset = 0f;
        m_LeftCharacter.offset = 0f;
        if (m_RightCharacter.position != CharacterPosition.BorderRight)
        {
            CharacterPosition destPos = GetCharacterDestPos(m_RightCharacter, true);
            StartCoroutine(Swipe(m_RightCharacter, destPos));
        }
        else
        {
            StartCoroutine(centerCharacter(m_RightCharacter));
        }
        if (m_LeftCharacter.position != CharacterPosition.BorderLeft)
        {
            CharacterPosition destPos = GetCharacterDestPos(m_LeftCharacter, false);
            StartCoroutine(Swipe(m_LeftCharacter, destPos));
        }
        else
        {
            StartCoroutine(centerCharacter(m_LeftCharacter));
        }
    }

    private void OnInputSwipeRight(InputSwipeRightEvent e)
    {
        if (CharacterLinesDistance() == 1 && m_RightCharacter.position == CharacterPosition.BorderRight)
        {
            if (m_RightCharacter.gameObject.activeSelf)
            {
                m_LeftCharacter.offset = m_LeftCharacter.initialOffset;
            }
            m_RightCharacter.offset = m_RightCharacter.initialOffset;
            StartCoroutine(RealignCharacter(m_RightCharacter));
        }
        if (m_RightCharacter.position != CharacterPosition.BorderRight)
        {
            CharacterPosition destPos = GetCharacterDestPos(m_RightCharacter, true);
            StartCoroutine(Swipe(m_RightCharacter, destPos));
        }
        if (m_LeftCharacter.position != CharacterPosition.BorderRight)
        {
            CharacterPosition destPos = GetCharacterDestPos(m_LeftCharacter, true);
            StartCoroutine(Swipe(m_LeftCharacter, destPos));
        }
    }

    private void OnInputSwipeLeft(InputSwipeLeftEvent e)
    {
        if (CharacterLinesDistance() == 1 && m_LeftCharacter.position == CharacterPosition.BorderLeft)
        {
            if (m_LeftCharacter.gameObject.activeSelf)
            {
                m_RightCharacter.offset = m_RightCharacter.initialOffset;
            }
            m_LeftCharacter.offset = m_LeftCharacter.initialOffset;
            StartCoroutine(RealignCharacter(m_LeftCharacter));
        }
        if (m_RightCharacter.position != CharacterPosition.BorderLeft)
        {
            CharacterPosition destPos = GetCharacterDestPos(m_RightCharacter, false);
            StartCoroutine(Swipe(m_RightCharacter, destPos));
        }
        if (m_LeftCharacter.position != CharacterPosition.BorderLeft)
        {
            CharacterPosition destPos = GetCharacterDestPos(m_LeftCharacter, false);
            StartCoroutine(Swipe(m_LeftCharacter, destPos));
        }
    }

    private IEnumerator Swipe(PlayerCharacter character, CharacterPosition destPos)
    {
        float startTransition = Time.time;
        Vector3 startPos = character.gameObject.transform.localPosition;
        Vector3 endPos = Vector3.right * ((int)destPos + character.offset);
        while (Time.time <= startTransition + m_TransitionDelay)
        {
            float coef = (Time.time - startTransition) / m_TransitionDelay;
            Vector3 newPos = Vector3.Lerp(startPos, endPos, coef);
            character.gameObject.transform.localPosition = newPos;
            yield return null;
        }
        character.gameObject.transform.localPosition = endPos;
        character.position = destPos;
    }

    private int CharacterLinesDistance()
    {
        return Mathf.Abs(((m_RightCharacter.position + 3) - (m_LeftCharacter.position + 3)) / 2);
    }

    private IEnumerator RealignCharacter(PlayerCharacter character)
    {
        float startTransition = Time.time;
        Vector3 startPos = character.gameObject.transform.localPosition;
        Vector3 endPos = character.gameObject.transform.localPosition + Vector3.right * character.offset;
        while (Time.time <= startTransition + m_TransitionDelay)
        {
            float coef = (Time.time - startTransition) / m_TransitionDelay;
            Vector3 newPos = Vector3.Lerp(startPos, endPos, coef);
            character.gameObject.transform.localPosition = newPos;
            yield return null;
        }
        character.gameObject.transform.localPosition = endPos;
    }

    private IEnumerator centerCharacter(PlayerCharacter character)
    {
        float startTransition = Time.time;
        Vector3 startPos = character.gameObject.transform.localPosition;
        Vector3 endPos = new Vector3((float)character.position, startPos.y, startPos.z);
        while (Time.time <= startTransition + m_TransitionDelay)
        {
            float coef = (Time.time - startTransition) / m_TransitionDelay;
            Vector3 newPos = Vector3.Lerp(startPos, endPos, coef);
            character.gameObject.transform.localPosition = newPos;
            yield return null;
        }
        character.gameObject.transform.localPosition = endPos;
    }

    private CharacterPosition GetCharacterDestPos(PlayerCharacter character, bool goingRight)
    {
        if (goingRight)
        {
            switch (character.position)
            {

                case CharacterPosition.BorderLeft:
                    return CharacterPosition.Left;
                case CharacterPosition.Left:
                    return CharacterPosition.Right;
                case CharacterPosition.Right:
                    return CharacterPosition.BorderRight;
                default:
                    return CharacterPosition.BorderRight;
            }
        }
        else
        {
            switch (character.position)
            {
                case CharacterPosition.BorderRight:
                    return CharacterPosition.Right;
                case CharacterPosition.Right:
                    return CharacterPosition.Left;
                case CharacterPosition.Left:
                    return CharacterPosition.BorderLeft;
                default:
                    return CharacterPosition.BorderLeft;
            }
        }
    }

    private void Update()
    {
        if (!GameManager.Instance.IsPlaying)
        {
            return;
        }
        UpdateCharacterheight();
    }
    private void UpdateCharacterheight()
    {
        LayerMask layer = LayerMask.GetMask("World");
        RaycastHit hit;
        if (Physics.Raycast(m_RightCharacter.gameObject.transform.position + Vector3.up * 30f, Vector3.down, out hit, 70f, layer))
        {
            Vector3 pos = m_RightCharacter.gameObject.transform.localPosition;
            m_RightCharacter.gameObject.transform.localPosition = new Vector3(pos.x, hit.point.y, pos.z);
        }
        if (Physics.Raycast(m_LeftCharacter.gameObject.transform.position + Vector3.up * 30f, Vector3.down, out hit, 70f, layer))
        {
            Vector3 pos = m_LeftCharacter.gameObject.transform.localPosition;
            m_LeftCharacter.gameObject.transform.localPosition = new Vector3(pos.x, hit.point.y, pos.z);
        }
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<InputSwipeLeftEvent>(OnInputSwipeLeft);
        EventManager.Instance.RemoveListener<InputSwipeRightEvent>(OnInputSwipeRight);
        EventManager.Instance.RemoveListener<InputSwipeUpEvent>(OnInputSwipeUp);
        EventManager.Instance.RemoveListener<InputSwipeDownEvent>(OnInputSwipeDown);
        EventManager.Instance.RemoveListener<CharacterDeadEvent>(OnCharacterDead);
    }
}
