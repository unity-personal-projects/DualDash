﻿using UnityEngine;
using DualDash.Events;
using System;
using System.Collections;

public enum CharacterType
{
    Right,
    Left
}

public class Character : MonoBehaviour
{
    [SerializeField] private CharacterType m_type = CharacterType.Right;

    public CharacterType Type { get => m_type; }


    private bool m_ShieldActive = false;
    public bool ShieldActive { get => m_ShieldActive; set => m_ShieldActive = value; }
    private float m_ShieldStartTime = 0f;
    [SerializeField] private GameObject m_ShieldOnPlayer = null;

    private void Awake()
    {
        EventManager.Instance.AddListener<GamePlayEvent>(OnGamePlay);
        EventManager.Instance.AddListener<PlayerGetPowerUpEvent>(OnPlayerGetPowerUp);
    }

    private void OnPlayerGetPowerUp(PlayerGetPowerUpEvent e)
    {
        if (gameObject.activeSelf)
        {
            switch (e.powerUpType)
            {
                case PowerUpType.Shield:
                    StartCoroutine(ActiveShield());
                    break;
            }
        }
    }

    private IEnumerator ActiveShield()
    {
        m_ShieldActive = true;
        m_ShieldStartTime = Time.time;
        m_ShieldOnPlayer.SetActive(true);
        while (Time.time < m_ShieldStartTime + GameManager.Instance.ShieldDuration)
        {
            m_ShieldOnPlayer.transform.LookAt(Camera.main.transform);
            yield return null;
            if (!ShieldActive)
            {
                break;
            }
        }
        m_ShieldOnPlayer.SetActive(false);
        m_ShieldActive = false;

    }

    private void OnGamePlay(GamePlayEvent e)
    {
        m_ShieldActive = false;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
        {
            if (ShieldActive)
            {
                ShieldActive = false;
                EventManager.Instance.Raise(new CharacterShieldDestroyObstacleEvent() { objectToDestroy = other.collider.gameObject });
            }
            else
            {
                EventManager.Instance.Raise(new CharacterDeadEvent() { character = gameObject });
            }
        }
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<GamePlayEvent>(OnGamePlay);
        EventManager.Instance.RemoveListener<PlayerGetPowerUpEvent>(OnPlayerGetPowerUp);
    }
}
