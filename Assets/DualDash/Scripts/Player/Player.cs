﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using DualDash.Events;

public class Player : Singleton<Player>
{
    private float m_Speed = 0f;
    public float Speed { get => m_Speed; set => m_Speed = value; }

    [SerializeField] private float m_StartSpeed = 7f;
    public float StartSpeed { get => m_StartSpeed; set => m_StartSpeed = value; }

    [SerializeField] private float m_MidSpeed = 14f;
    [SerializeField] private float m_MaxSpeed = 20f;

    [SerializeField] private GameObject m_CharactersPrefab = null;
    private GameObject m_Characters = null;

    private bool m_MagnetActive = false;
    public bool MagnetActive { get => m_MagnetActive; set => m_MagnetActive = value; }

    [SerializeField] private GameObject m_MagnetSpherePrefab = null;

    protected override void Awake()
    {
        base.Awake();
        m_Speed = m_StartSpeed;
        EventManager.Instance.AddListener<GamePlayEvent>(OnGamePlay);
        EventManager.Instance.AddListener<PlayerGetPowerUpEvent>(OnPlayerGetPowerUp);
    }

    private void OnPlayerGetPowerUp(PlayerGetPowerUpEvent e)
    {
        switch (e.powerUpType)
        {
            case PowerUpType.Magnet:
                if (!MagnetActive)
                {
                    StartCoroutine(ActiveMagnet());
                }
                break;
            case PowerUpType.Shield:
                ActiveShield();
                break;
        }
    }

    private void ActiveShield()
    {
        Character[] wolfs = FindObjectsOfType<Character>();
        foreach (Character wolf in wolfs)
        {
            EventManager.Instance.Raise(new ShieldUpEvent());
        }
    }

    private IEnumerator ActiveMagnet()
    {
        Character[] wolfs = FindObjectsOfType<Character>();
        m_MagnetActive = true;
        List<GameObject> spheres = new List<GameObject>();
        foreach (Character wolf in wolfs)
        {
            GameObject g = Instantiate(m_MagnetSpherePrefab);
            g.transform.SetParent(wolf.transform);
            g.transform.localPosition = Vector3.zero;
            spheres.Add(g);
        }
        yield return new WaitForSeconds(GameManager.Instance.MagnetDuration);
        m_MagnetActive = false;
        foreach (GameObject sphere in spheres)
        {
            Destroy(sphere);
        }
    }

    private void OnGamePlay(GamePlayEvent e)
    {
        m_MagnetActive = false;
        transform.position = Vector3.zero;
        if (m_Characters != null)
        {
            Destroy(m_Characters);
        }
        m_Characters = Instantiate(m_CharactersPrefab);
        m_Characters.transform.SetParent(transform);
    }

    private void Update()
    {
        if (!GameManager.Instance.IsPlaying)
        {
            return;
        }
        float timeSinceStart = (Time.time - GameManager.Instance.GameStartTime);
        if (m_Speed < m_MidSpeed)
        {
            m_Speed = m_StartSpeed + 0.5f * (float)Math.Truncate(timeSinceStart / 10f);
        }
        else if (m_StartSpeed < m_MaxSpeed)
        {
            m_Speed = m_StartSpeed + 0.5f * (float)Math.Truncate(timeSinceStart / 15f);
        }
        else
        {
            m_Speed = m_MaxSpeed;
        }
        transform.Translate(Vector3.forward * Speed * Time.deltaTime);
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<GamePlayEvent>(OnGamePlay);
        EventManager.Instance.RemoveListener<PlayerGetPowerUpEvent>(OnPlayerGetPowerUp);
    }
}
