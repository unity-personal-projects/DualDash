﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHolder : MonoBehaviour
{
    [SerializeField] private float m_Duration = 0.4f;
    private float m_Offset = 0f;

    private Coroutine m_AdaptCameraCoroutine = null;
    private void Awake()
    {
        m_Offset = transform.position.y;
    }
    void Update()
    {
        if (!GameManager.Instance.IsPlaying)
        {
            return;
        }

        RaycastHit hit;
        LayerMask layer = LayerMask.GetMask("World");
        if (Physics.Raycast(transform.position + Vector3.up * 30f, Vector3.down, out hit, 70f, layer))
        {
            float distance = Vector3.Distance(transform.position, hit.point);
            if ((distance < 8 || distance > 10) && (m_AdaptCameraCoroutine == null))
            {
                m_AdaptCameraCoroutine = StartCoroutine(AdaptCamera(hit.point));
            }
        }
    }
    private IEnumerator AdaptCamera(Vector3 dest)
    {
        float startTime = Time.time;
        Vector3 startPosition = transform.localPosition;
        Vector3 endPosition = new Vector3(0, dest.y + m_Offset, transform.localPosition.z);
        while (Time.time < startTime + m_Duration)
        {
            float k = (Time.time - startTime) / m_Duration;
            transform.localPosition = Vector3.Lerp(startPosition, endPosition, k);
            yield return null;
        }
        transform.localPosition = endPosition;
        m_AdaptCameraCoroutine = null;

    }
}
