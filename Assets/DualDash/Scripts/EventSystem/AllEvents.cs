﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DualDash.Events;

#region GameManager Events

public class GameMenuEvent : DualDash.Events.Event { }
public class GamePlayEvent : DualDash.Events.Event { }
public class GamePauseEvent : DualDash.Events.Event { }
public class GameResumeEvent : DualDash.Events.Event { }
public class GameOverEvent : DualDash.Events.Event { }
public class GameVictoryEvent : DualDash.Events.Event { }
#endregion

#region GameState Events
public class MainMenuButtonClickedEvent : DualDash.Events.Event { }
public class PlayButtonClickedEvent : DualDash.Events.Event { }
public class ResumeButtonClickedEvent : DualDash.Events.Event { }
public class PauseButtonClickedEvent : DualDash.Events.Event { }
public class PlayerDeadEvent : DualDash.Events.Event { }

#endregion

#region Input Events

public class InputTapEvent : DualDash.Events.Event
{
    public Vector2 position;
}

public class InputSwipeDownEvent : DualDash.Events.Event { }

public class InputSwipeUpEvent : DualDash.Events.Event { }

public class InputSwipeLeftEvent : DualDash.Events.Event { }

public class InputSwipeRightEvent : DualDash.Events.Event { }

#endregion

#region Obstacles Events

public class ObstacleDestructionEvent : DualDash.Events.Event
{
    public GameObject obstacle;
}

public class CharacterShieldDestroyObstacleEvent : DualDash.Events.Event
{
    public GameObject objectToDestroy;
}

#endregion

#region Player Events

public class CharacterDeadEvent : DualDash.Events.Event
{
    public GameObject character;
}

public class ShieldUpEvent : DualDash.Events.Event { }

#endregion

#region Objects Events

public class PlayerGetPowerUpEvent : DualDash.Events.Event
{
    public PowerUpType powerUpType;
    public PieceValue pieceType;
}
public class DestroyPowerUpEvent : DualDash.Events.Event
{
    public GameObject powerUp;
}

#endregion