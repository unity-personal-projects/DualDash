﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DualDash.Events;

public enum PowerUpType
{
    Piece,
    Magnet,
    Shield,
    Boost,
}

public enum PieceValue
{
    Rabbit = 1,
    Chicken = 2,
    Pig = 5,
    Sheep = 10,
}

public class PowerUp : MonoBehaviour
{
    [SerializeField] private PowerUpType m_Type = PowerUpType.Piece;
    public PowerUpType Type { get => m_Type; set => m_Type = value; }

    [SerializeField] private PieceValue m_PieceType = PieceValue.Rabbit;
    public PieceValue PieceType { get => m_PieceType; set => m_PieceType = value; }


    private CharacterPosition m_PathPosition = CharacterPosition.BorderRight;
    public CharacterPosition PathPosition { get => m_PathPosition; set => m_PathPosition = value; }

    [SerializeField] private GameObject m_RabbitGO = null;
    [SerializeField] private GameObject m_ChickenGO = null;
    [SerializeField] private GameObject m_PigGO = null;
    [SerializeField] private GameObject m_SheepGO = null;
    [SerializeField] private GameObject m_ScoreUpdateElement = null;

    private void Start()
    {
        transform.localPosition = new Vector3((float)PathPosition, 30f, transform.localPosition.z);
        RaycastHit hit;
        LayerMask layer = LayerMask.GetMask("PowerUp") + LayerMask.GetMask("Obstacle");
        if (Physics.Raycast(transform.position + Vector3.up * 30f, Vector3.down, out hit, 70f, layer)
        || Physics.Raycast(transform.position + Vector3.up * 30f + Vector3.forward, Vector3.down, out hit, 70f, layer)
        || Physics.Raycast(transform.position + Vector3.up * 30f - Vector3.forward, Vector3.down, out hit, 70f, layer))
        {
            if (Type == PowerUpType.Piece)
            {
                EventManager.Instance.Raise(new DestroyPowerUpEvent() { powerUp = gameObject });
                return;
            }
            transform.position += Vector3.forward * 4f;
        }

        if (Physics.Raycast(transform.position + Vector3.up * 30f, Vector3.down, out hit, 70f, LayerMask.GetMask("World")))
        {
            float distance = Vector3.Distance(transform.position, hit.point);
            transform.localPosition = new Vector3((float)PathPosition, hit.point.y, transform.localPosition.z);
        }
        else
        {
            EventManager.Instance.Raise(new DestroyPowerUpEvent() { powerUp = gameObject });
        }
        float rotationAngle = Random.Range(0f, 360f);
        transform.localRotation = Quaternion.AngleAxis(rotationAngle, Vector3.up);
        if (Type == PowerUpType.Piece)
        {
            DefinePieceGO();
            StartCoroutine(ListenToMagnet());
        }
        else
        {
            StartCoroutine(Rotate());
        }
    }

    private IEnumerator ListenToMagnet()
    {
        bool attractedToMagnet = false;
        while (!attractedToMagnet)
        {
            yield return null;
            Character[] wolfs = FindObjectsOfType<Character>();
            foreach (Character wolf in wolfs)
            {
                if (wolf.gameObject.activeSelf && Player.Instance.MagnetActive && Vector3.Distance(transform.position, wolf.transform.position) < 5f)
                {
                    StartCoroutine(PieceAttractedToPlayer(wolf.transform));
                    attractedToMagnet = true;
                    break;
                }
            }
        }
    }

    private IEnumerator PieceAttractedToPlayer(Transform wolfTransform)
    {
        while (true)
        {
            if (!wolfTransform.gameObject.activeSelf)
            {
                EventManager.Instance.Raise(new DestroyPowerUpEvent() { powerUp = gameObject });
            }
            transform.Translate((wolfTransform.position - transform.position).normalized * 50f * Time.deltaTime, Space.World);
            yield return null;
        }
    }

    private IEnumerator Rotate()
    {
        while (true)
        {
            transform.rotation *= Quaternion.AngleAxis(60f * Time.deltaTime, Vector3.up);
            yield return null;
        }
    }

    private void DefinePieceGO()
    {
        GameObject g = null;
        switch (PieceType)
        {
            case PieceValue.Rabbit:
                g = Instantiate(m_RabbitGO);
                break;
            case PieceValue.Chicken:
                g = Instantiate(m_ChickenGO);
                break;
            case PieceValue.Pig:
                g = Instantiate(m_PigGO);
                break;
            default:
                g = Instantiate(m_SheepGO);
                break;
        }
        g.transform.SetParent(transform);
        g.transform.localPosition = Vector3.zero;
        g.transform.localRotation = Quaternion.identity;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<Character>())
        {
            EventManager.Instance.Raise(new PlayerGetPowerUpEvent() { powerUpType = Type, pieceType = PieceType });
            if (Type == PowerUpType.Piece)
            {
                GameObject g = Instantiate(m_ScoreUpdateElement);
                g.GetComponent<ScoreUpdateElement>().PieceType = m_PieceType;
                g.transform.position = transform.position + Vector3.up * 0.8f;
            }
            EventManager.Instance.Raise(new DestroyPowerUpEvent() { powerUp = gameObject });
        }
        if (other.gameObject.GetComponent<ObstacleDestructor>())
        {
            EventManager.Instance.Raise(new DestroyPowerUpEvent() { powerUp = gameObject });
        }
    }
}
