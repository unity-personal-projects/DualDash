﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUpdateElement : MonoBehaviour
{
    [SerializeField] private PieceValue m_PieceType = PieceValue.Rabbit;
    public PieceValue PieceType { get => m_PieceType; set => m_PieceType = value; }

    [SerializeField] private Text m_Text = null;

    [SerializeField] private float m_TransitionDuration = 0.8f;
    [SerializeField] private Vector2 m_ScaleOverTime = new Vector2(0.8f, 1.4f);

    private IEnumerator Start()
    {
        m_Text.text = "+ " + ((int)m_PieceType * GameManager.Instance.BonusDistance).ToString();
        Vector3 startPos = transform.position;
        Vector3 endPos = transform.position + Vector3.up * 3f;
        Color startColor = m_Text.color;
        Color endColor = m_Text.color;
        endColor.a = 0;
        float startTime = Time.time;
        while (Time.time < startTime + m_TransitionDuration)
        {
            float k = (Time.time - startTime) / m_TransitionDuration;
            transform.position = Vector3.Lerp(startPos, endPos, k);
            transform.localScale = Vector3.one * Mathf.Lerp(m_ScaleOverTime.x, m_ScaleOverTime.y, k);
            m_Text.color = Color.Lerp(startColor, endColor, k < 0.5 ? 0 : (k - 0.5f) * 2f);
            yield return null;
        }
        Destroy(gameObject);
    }
}
