﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DualDash.Events;
using System;

public enum Playlist
{
    Menu,
    GamePlay,
    GameOver,
}

public class MusicLoopManager : Singleton<MusicLoopManager>
{
    [SerializeField] private List<AudioClip> m_MenuPlaylist = new List<AudioClip>();
    [SerializeField] private List<AudioClip> m_GamePlayPlaylist = new List<AudioClip>();
    [SerializeField] private List<AudioClip> m_GameOverPlaylist = new List<AudioClip>();

    private Playlist m_PlayingPlaylist = Playlist.Menu;
    private AudioSource m_PlayerAudioSource = null;
    private Coroutine m_PlayingMusicsCoroutine = null;

    private bool m_Pause = false;

    protected override void Awake()
    {
        base.Awake();
        EventManager.Instance.AddListener<GameMenuEvent>(GameMenu);
        EventManager.Instance.AddListener<GamePlayEvent>(GamePlay);
        EventManager.Instance.AddListener<GamePauseEvent>(GamePause);
        EventManager.Instance.AddListener<GameResumeEvent>(GameResume);
        EventManager.Instance.AddListener<GameOverEvent>(GameOver);
        Player player = GameObject.FindObjectOfType<Player>();
        if (player != null)
        {
            m_PlayerAudioSource = player.GetComponentInChildren<AudioSource>();
        }
    }
    
    private void Start()
    {
        if (m_PlayingPlaylist != Playlist.Menu)
        {
            m_PlayingPlaylist = Playlist.Menu;
            ChangePlaylist(m_MenuPlaylist);
        }
    }

    private IEnumerator PlayPlaylist(List<AudioClip> playlist)
    {
        while (true)
        {
            foreach (AudioClip clip in playlist)
            {
                while (m_Pause)
                {
                    yield return null;
                }
                m_PlayerAudioSource.clip = clip;
                m_PlayerAudioSource.Play();
                yield return new WaitForSecondsRealtime(clip.length);
            }
        }
    }

    private void ChangePlaylist(List<AudioClip> nextPlaylist)
    {
        if (m_PlayingMusicsCoroutine != null)
        {
            StopCoroutine(m_PlayingMusicsCoroutine);
        }
        m_PlayingMusicsCoroutine = StartCoroutine(PlayPlaylist(nextPlaylist));
    }

    private void GameMenu(GameMenuEvent e)
    {
        if (m_PlayingPlaylist != Playlist.Menu)
        {
            m_PlayingPlaylist = Playlist.Menu;
            ChangePlaylist(m_MenuPlaylist);
        }
    }

    private void GameOver(GameOverEvent e)
    {
        if (m_PlayingPlaylist != Playlist.GameOver)
        {
            m_PlayingPlaylist = Playlist.GameOver;
            ChangePlaylist(m_GameOverPlaylist);
        }
    }

    private void GameResume(GameResumeEvent e)
    {
        if (m_PlayingPlaylist != Playlist.GamePlay)
        {
            m_PlayingPlaylist = Playlist.GamePlay;
            ChangePlaylist(m_GamePlayPlaylist);
        }
    }

    private void GamePause(GamePauseEvent e)
    {
        m_Pause = true;
    }

    private void GamePlay(GamePlayEvent e)
    {
        if (m_PlayingPlaylist != Playlist.GamePlay)
        {
            m_PlayingPlaylist = Playlist.GamePlay;
            ChangePlaylist(m_GamePlayPlaylist);
        }
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<GameMenuEvent>(GameMenu);
        EventManager.Instance.RemoveListener<GamePlayEvent>(GamePlay);
        EventManager.Instance.RemoveListener<GamePauseEvent>(GamePause);
        EventManager.Instance.RemoveListener<GameResumeEvent>(GameResume);
        EventManager.Instance.RemoveListener<GameOverEvent>(GameOver);
    }
}
