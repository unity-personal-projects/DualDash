﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DualDash.Events;
using System;

public enum GameState
{
    gameMenu,
    gamePlay,
    gamePause,
    gameOver,
    gameVictory
}

public class GameManager : Singleton<GameManager>
{
    private GameState m_GameState;
    public bool IsPlaying { get { return m_GameState == GameState.gamePlay; } }
    public GameState GameState { get => m_GameState; set => m_GameState = value; }
    private float m_GameStartTime = 0f;
    public float GameStartTime { get { return m_GameStartTime; } }


    private float m_BonusDistance = 1f;
    public float BonusDistance { get => m_BonusDistance; set => m_BonusDistance = value; }

    private float m_BestScore = 0f;
    public float BestScore { get => m_BestScore; set => m_BestScore = value; }

    private float m_ShieldDuration = 12f;
    public float ShieldDuration { get => m_ShieldDuration; set => m_ShieldDuration = value; }

    private float m_MagnetDuration = 5f;
    public float MagnetDuration { get => m_MagnetDuration; set => m_MagnetDuration = value; }

    private float m_Score = 0f;
    public float Score
    {
        get { return m_Score; }
        set
        {
            m_Score = value;
            m_BestScore = Mathf.Max(m_BestScore, value);
        }
    }

    private float m_Pieces = 0f;
    public float Pieces { get => m_Pieces; set => m_Pieces = value; }
    private float m_TotalPieces = 0f;
    public float TotalPieces { get => m_TotalPieces; set => m_TotalPieces = value; }

    protected override void Awake()
    {
        base.Awake();
        InitNewGame();
        EventManager.Instance.AddListener<MainMenuButtonClickedEvent>(OnMainMenuButtonClicked);
        EventManager.Instance.AddListener<PlayButtonClickedEvent>(OnPlayButtonClicked);
        EventManager.Instance.AddListener<ResumeButtonClickedEvent>(OnResumeButtonClicked);
        EventManager.Instance.AddListener<PauseButtonClickedEvent>(OnPauseButtonClicked);
        EventManager.Instance.AddListener<PlayerDeadEvent>(OnPlayerDead);
    }

    private void Update()
    {
        if (!IsPlaying)
        {
            return;
        }
    }

    void InitNewGame()
    {
        m_GameStartTime = Time.time;
        m_Pieces = 0f;
        m_Score = 0f;
        Menu();
    }

    #region Callbacks to Events issued by MenuManager
    private void OnMainMenuButtonClicked(MainMenuButtonClickedEvent e)
    {
        Menu();
    }

    private void OnPlayButtonClicked(PlayButtonClickedEvent e)
    {

        Play();
    }

    private void OnResumeButtonClicked(ResumeButtonClickedEvent e)
    {
        Resume();
    }

    private void OnPauseButtonClicked(PauseButtonClickedEvent e)
    {
        if (IsPlaying)
        {
            Pause();
        }
    }

    private void OnPlayerDead(PlayerDeadEvent e)
    {
        Over();
    }
    #endregion

    #region GameState methods
    private void Menu()
    {
        Time.timeScale = 0;
        m_GameState = GameState.gameMenu;

        EventManager.Instance.Raise(new GameMenuEvent());
    }

    private void Play()
    {
        InitNewGame();
        Time.timeScale = 1;
        m_GameState = GameState.gamePlay;

        EventManager.Instance.Raise(new GamePlayEvent());
    }

    private void Pause()
    {
        if (!IsPlaying) return;

        Time.timeScale = 0;
        m_GameState = GameState.gamePause;
        EventManager.Instance.Raise(new GamePauseEvent());
    }

    private void Resume()
    {
        if (IsPlaying) return;

        Time.timeScale = 1;
        m_GameState = GameState.gamePlay;
        EventManager.Instance.Raise(new GameResumeEvent());
    }

    private void Over()
    {
        Time.timeScale = 0;
        m_GameState = GameState.gameOver;
        EventManager.Instance.Raise(new GameOverEvent());
    }
    #endregion
    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<MainMenuButtonClickedEvent>(OnMainMenuButtonClicked);
        EventManager.Instance.RemoveListener<PlayButtonClickedEvent>(OnPlayButtonClicked);
        EventManager.Instance.RemoveListener<ResumeButtonClickedEvent>(OnResumeButtonClicked);
        EventManager.Instance.RemoveListener<PauseButtonClickedEvent>(OnPauseButtonClicked);
        EventManager.Instance.RemoveListener<PlayerDeadEvent>(OnPlayerDead);
    }
}
