﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DualDash.Events;

public class ObstacleManager : Singleton<ObstacleManager>
{
    [SerializeField] private GameObject m_ObstaclePrefab = null;

    [SerializeField] private Vector2 m_SpawnCooldown = new Vector2(1f, 1.7f);

    [SerializeField] private float m_DistanceSpawnFromPlayer = 75f;
    private List<GameObject> m_ObstaclesInGame = new List<GameObject>();

    private Transform m_Player = null;
    private Coroutine m_SpawnCoroutine = null;

    protected override void Awake()
    {
        base.Awake();
        Player player = FindObjectOfType<Player>();
        if (player != null)
        {
            m_Player = player.transform;
        }
        EventManager.Instance.AddListener<GamePlayEvent>(OnGamePlay);
        EventManager.Instance.AddListener<ObstacleDestructionEvent>(OnObstacleDestruction);
        EventManager.Instance.AddListener<CharacterShieldDestroyObstacleEvent>(OnCharacterShieldDestroyObstacle);
    }

    private void OnCharacterShieldDestroyObstacle(CharacterShieldDestroyObstacleEvent e)
    {
        bool objectFound = false;
        foreach (GameObject obstacle in m_ObstaclesInGame)
        {
            foreach (Collider c in obstacle.GetComponentsInChildren<Collider>())
            {
                if (e.objectToDestroy.GetInstanceID() == c.gameObject.GetInstanceID())
                {
                    Destroy(c.gameObject);
                    objectFound = true;
                    break;
                }
            }
            if (objectFound)
            {
                break;
            }
        }
    }

    private void OnObstacleDestruction(ObstacleDestructionEvent e)
    {
        m_ObstaclesInGame.Remove(e.obstacle);
        Destroy(e.obstacle);
    }

    private void OnGamePlay(GamePlayEvent e)
    {
        foreach (GameObject g in m_ObstaclesInGame)
        {
            Destroy(g);
        }
        m_ObstaclesInGame = new List<GameObject>();
        if (m_SpawnCoroutine != null)
        {
            StopCoroutine(m_SpawnCoroutine);
            m_SpawnCoroutine = null;
        }
        m_SpawnCoroutine = StartCoroutine(StartSpawing());
    }

    private IEnumerator StartSpawing()
    {
        yield return new WaitForSeconds(1f);
        while (true)
        {
            if (!GameManager.Instance.IsPlaying)
            {
                break;
            }
            GameObject g = Instantiate(m_ObstaclePrefab);
            g.transform.position = Vector3.forward * (m_Player.position.z + m_DistanceSpawnFromPlayer);
            g.transform.SetParent(transform);
            m_ObstaclesInGame.Add(g);
            yield return new WaitForSeconds(Random.Range(m_SpawnCooldown.x, m_SpawnCooldown.y) / (Player.Instance.Speed / Player.Instance.StartSpeed));
        }
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<GamePlayEvent>(OnGamePlay);
        EventManager.Instance.RemoveListener<ObstacleDestructionEvent>(OnObstacleDestruction);
        EventManager.Instance.RemoveListener<CharacterShieldDestroyObstacleEvent>(OnCharacterShieldDestroyObstacle);
    }
}
