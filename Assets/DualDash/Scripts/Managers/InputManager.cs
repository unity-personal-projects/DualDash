﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DualDash.Events;

public class InputManager : Singleton<InputManager>
{
    private Touch m_Touch = new Touch();
    private Vector2 m_BeginTouchPosition, m_EndTouchPosition;


    [SerializeField] private int m_Magnitude = 125;

    [SerializeField] private float m_LastInputCooldown = 0.1f;
    private float m_LastInputTime = -1f;

    private void Update()
    {
        if (!GameManager.Instance.IsPlaying)
        {
            return;
        }

        if (Input.touchCount > 0)
        {
            m_Touch = Input.GetTouch(0);
            switch (m_Touch.phase)
            {
                case TouchPhase.Began:
                    m_BeginTouchPosition = m_Touch.position;
                    break;
                case TouchPhase.Ended:
                    m_EndTouchPosition = m_Touch.position;
                    if (Time.time > m_LastInputTime + m_LastInputCooldown)
                    {
                        if (Vector2.Distance(m_BeginTouchPosition, m_EndTouchPosition) < m_Magnitude) // Tap
                        {
                            EventManager.Instance.Raise(new InputTapEvent() { position = m_BeginTouchPosition });
                        }
                        else // Swipe
                        {
                            Vector2 delta = m_EndTouchPosition - m_BeginTouchPosition;
                            float x = delta.x;
                            float y = delta.y;
                            if (Mathf.Abs(x) > Mathf.Abs(y))
                            {
                                if (x < 0)
                                {
                                    EventManager.Instance.Raise(new InputSwipeLeftEvent());
                                }
                                else
                                {
                                    EventManager.Instance.Raise(new InputSwipeRightEvent());
                                }
                            }
                            else
                            {
                                if (y < 0)
                                {
                                    EventManager.Instance.Raise(new InputSwipeDownEvent());
                                }
                                else
                                {
                                    EventManager.Instance.Raise(new InputSwipeUpEvent());
                                }
                            }
                        }
                        m_LastInputTime = Time.time;
                    }
                    break;
            }
        }

    }
}
