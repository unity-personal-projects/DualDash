﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DualDash.Events;
using UnityEngine.UI;
using System;

public class MenuManager : Singleton<MenuManager>
{
    [Header("Panels")]
    [SerializeField] private GameObject m_PanelMainMenu = null;
    [SerializeField] private GameObject m_PanelInGameMenu = null;
    [SerializeField] private GameObject m_PanelHUD = null;
    [SerializeField] private GameObject m_PanelGameOver = null;
    [SerializeField] private GameObject m_PanelHowToPlay = null;

    [SerializeField] private Text m_CurrentScore = null;
    [SerializeField] private Text m_CurrentPigs = null;
    [SerializeField] private Text m_GameOverDistance = null;
    [SerializeField] private Text m_GameOverPreysCollected = null;
    [SerializeField] private Text m_GameOverTotalPreys = null;

    [SerializeField] private Text m_ShieldIncreaseText = null;
    [SerializeField] private Text m_MagnetIncreaseText = null;
    [SerializeField] private Text m_BonusDistanceText = null;

    private List<GameObject> m_AllPanels = new List<GameObject>();
    private Transform m_Player = null;
    protected override void Awake()
    {
        base.Awake();
        EventManager.Instance.AddListener<GameMenuEvent>(GameMenu);
        EventManager.Instance.AddListener<GamePlayEvent>(GamePlay);
        EventManager.Instance.AddListener<GamePauseEvent>(GamePause);
        EventManager.Instance.AddListener<GameResumeEvent>(GameResume);
        EventManager.Instance.AddListener<GameOverEvent>(GameOver);
        Player player = FindObjectOfType<Player>();
        if (player != null)
        {
            m_Player = player.transform;
        }
        RegisterPanels();
    }

    private void Start()
    {
        OpenPanel(m_PanelMainMenu);
    }

    private void Update()
    {
        if (!GameManager.Instance.IsPlaying)
        {
            return;
        }
        GameManager.Instance.Score = m_Player.position.z;
        m_CurrentScore.text = Convert.ToString(Convert.ToInt32(GameManager.Instance.Score)) + " m";

        m_CurrentPigs.text = Convert.ToString(GameManager.Instance.Pieces);

        GameManager.Instance.BonusDistance = 1f + 1f * (int)Math.Truncate(Player.Instance.transform.position.z / 500f);
        m_BonusDistanceText.text = "x" + GameManager.Instance.BonusDistance.ToString();
    }

    private void RegisterPanels()
    {
        m_AllPanels = new List<GameObject>
        {
            m_PanelMainMenu,
            m_PanelInGameMenu,
            m_PanelHUD,
            m_PanelGameOver,
            m_PanelHowToPlay
        };
    }

    private void OpenPanel(GameObject panel)
    {
        foreach (GameObject item in m_AllPanels)
        {
            if (item)
            {
                item.SetActive(item == panel);
            }
        }
    }

    public void PlayButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new PlayButtonClickedEvent());
    }

    public void ResumeButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new ResumeButtonClickedEvent());
    }

    public void MainMenuButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new MainMenuButtonClickedEvent());
    }

    public void PauseButtonHasBeenClicked()
    {
        EventManager.Instance.Raise(new PauseButtonClickedEvent());
    }

    public void HowToPlayButtonhasBeenClicked()
    {
        OpenPanel(m_PanelHowToPlay);
    }

    public void UpdateShieldHasBeenClicked()
    {
        if (GameManager.Instance.TotalPieces >= 100f)
        {
            GameManager.Instance.TotalPieces -= 100f;
            m_GameOverTotalPreys.text = Mathf.Round(GameManager.Instance.TotalPieces).ToString();

            GameManager.Instance.ShieldDuration += 0.5f;
            Debug.Log("Update shield\n" + GameManager.Instance.ShieldDuration + " + 0.5 seconds\nNeeds 100 preys");
            m_ShieldIncreaseText.text = "Update shield\n" + GameManager.Instance.ShieldDuration + " + 0.5 seconds\nNeeds 100 preys";
        }
    }

    public void UpdateMagnetHasBeenClicked()
    {
        if (GameManager.Instance.TotalPieces >= 100f)
        {
            GameManager.Instance.TotalPieces -= 100f;
            m_GameOverTotalPreys.text = Mathf.Round(GameManager.Instance.TotalPieces).ToString();

            GameManager.Instance.MagnetDuration += 0.5f;
            Debug.Log("Update magnet\n" + GameManager.Instance.MagnetDuration + " + 0.5 seconds\nNeeds 100 preys");
            m_MagnetIncreaseText.text = "Update magnet\n" + GameManager.Instance.MagnetDuration + " + 0.5 seconds\nNeeds 100 preys";
        }
    }

    private void DisableUpdates()
    {
        m_ShieldIncreaseText.transform.parent.gameObject.SetActive(false);
        m_MagnetIncreaseText.transform.parent.gameObject.SetActive(false);
    }

    private void GameMenu(GameMenuEvent e)
    {
        OpenPanel(m_PanelMainMenu);
        m_GameOverDistance.text = "0 m";
        m_GameOverPreysCollected.text = "0";
        m_GameOverTotalPreys.text = "0";

    }

    private void GamePlay(GamePlayEvent e)
    {
        OpenPanel(m_PanelHUD);
    }

    private void GamePause(GamePauseEvent e)
    {
        m_PanelHUD.SetActive(false);
        OpenPanel(m_PanelInGameMenu);
    }

    private void GameResume(GameResumeEvent e)
    {
        OpenPanel(m_PanelHUD);
    }

    private void GameOver(GameOverEvent e)
    {
        OpenPanel(m_PanelGameOver);
        StartCoroutine(BuildScore());
    }

    private IEnumerator BuildScore()
    {
        float delay = 2f;
        float distance = Player.Instance.transform.position.z;
        float elapsedTime = 0f;
        while (elapsedTime < delay)
        {
            float k = elapsedTime / delay;
            m_GameOverDistance.text = Mathf.Round(Mathf.Lerp(0f, distance, k)).ToString() + " m";
            yield return null;
            elapsedTime += Time.fixedUnscaledDeltaTime;
        }
        m_GameOverPreysCollected.text = Mathf.Round(distance).ToString() + " m";

        float preysCollected = GameManager.Instance.Pieces;
        elapsedTime = 0f;
        while (elapsedTime < delay)
        {
            float k = elapsedTime / delay;
            m_GameOverPreysCollected.text = Mathf.Round(Mathf.Lerp(0f, preysCollected, k)).ToString();
            yield return null;
            elapsedTime += Time.fixedUnscaledDeltaTime;
        }
        m_GameOverPreysCollected.text = Mathf.Round(preysCollected).ToString();

        float totalPreys = GameManager.Instance.TotalPieces;
        elapsedTime = 0f;
        while (elapsedTime < delay)
        {
            float k = elapsedTime / delay;
            m_GameOverTotalPreys.text = Mathf.Round(Mathf.Lerp(0f, totalPreys, k)).ToString();
            yield return null;
            elapsedTime += Time.fixedUnscaledDeltaTime;
        }
        m_GameOverTotalPreys.text = Mathf.Round(totalPreys).ToString();

        m_ShieldIncreaseText.text = "Update shield\n" + GameManager.Instance.ShieldDuration + " + 0.5 seconds\nNeeds 100 preys";
        m_MagnetIncreaseText.text = "Update magnet\n" + GameManager.Instance.MagnetDuration + " + 0.5 seconds\nNeeds 100 preys";
    }
    private void OnPlayerDead(PlayerDeadEvent e)
    {
        m_PanelMainMenu.SetActive(false);
        m_PanelInGameMenu.SetActive(false);
        m_PanelHUD.SetActive(false);
        m_PanelGameOver.SetActive(true);
    }

    public void OnDestroy()
    {
        EventManager.Instance.RemoveListener<GameMenuEvent>(GameMenu);
        EventManager.Instance.RemoveListener<GamePlayEvent>(GamePlay);
        EventManager.Instance.RemoveListener<GamePauseEvent>(GamePause);
        EventManager.Instance.RemoveListener<GameResumeEvent>(GameResume);
        EventManager.Instance.RemoveListener<GameOverEvent>(GameOver);
    }
}
