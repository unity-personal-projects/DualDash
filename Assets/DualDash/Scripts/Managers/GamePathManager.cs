﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DualDash.Events;

public class GamePathManager : Singleton<GamePathManager>
{
    [SerializeField] private List<GameObject> m_WallsPrefabs = new List<GameObject>();
    // private List<GameObject> m_Walls = new List<GameObject>();
    // [SerializeField]
    // private Transform m_WallsParent = null;

    [SerializeField] private List<GameObject> m_GroundsPrefabs = new List<GameObject>();
    [SerializeField] private Transform m_GroundParent = null;
    private List<GameObject> m_Grounds = new List<GameObject>();

    [SerializeField] private List<Transform> m_Paths = new List<Transform>();
    private Vector4[] m_PathsPositions;
    private Vector4[] m_PathsDirections;

    private Transform m_Player = null;

    protected override void Awake()
    {
        base.Awake();
        EventManager.Instance.AddListener<GamePlayEvent>(OnGamePlay);
        m_PathsPositions = new Vector4[m_Paths.Count];
        m_PathsDirections = new Vector4[m_Paths.Count];
        Player player = FindObjectOfType<Player>();
        if (player != null)
        {
            m_Player = player.transform;
        }
    }

    private void OnGamePlay(GamePlayEvent e)
    {
        DestroyAll();
        Initialize();
        TransferDataToShader();
    }

    private void Update()
    {
        if (!GameManager.Instance.IsPlaying)
        {
            return;
        }

        foreach (GameObject ground in m_Grounds)
        {
            if (ground.transform.localPosition.z < m_Player.position.z - 130f)
            {
                float groundOffset = (float)Math.Truncate(m_Player.position.z / 100f);
                ground.transform.localPosition = Vector3.forward * 100f * (groundOffset + m_Grounds.Count - 1);
                float angle = (float)(UnityEngine.Random.Range(0, 4)) * 90f;
                ground.transform.localRotation = Quaternion.AngleAxis(angle, Vector3.up);
            }
        }
    }
    private void TransferDataToShader()
    {
        for (int i = 0; i < m_Paths.Count; i++)
        {
            Transform path = m_Paths[i];
            m_PathsPositions[i] = path.position;
            m_PathsDirections[i] = path.forward;
        }

        Shader.SetGlobalInt("_NPaths", m_Paths.Count);
        Shader.SetGlobalVectorArray("_PathsPositions", m_PathsPositions);
        Shader.SetGlobalVectorArray("_PathsDirections", m_PathsDirections);
    }

    private void Initialize()
    {
        int i = 0;
        foreach (GameObject ground in m_GroundsPrefabs)
        {
            m_Grounds.Add(Instantiate(ground));
            m_Grounds[i].transform.SetParent(m_GroundParent);
            m_Grounds[i].transform.localPosition = Vector3.forward * 100f * (float)i;
            float angle = (float)(UnityEngine.Random.Range(0, 4)) * 90f;
            m_Grounds[i].transform.localRotation = Quaternion.AngleAxis(angle, Vector3.up);
            ++i;
        }
    }

    private void DestroyAll()
    {
        if (m_Grounds.Count != 0)
        {
            foreach (GameObject g in m_Grounds)
            {
                Destroy(g);
            }
            m_Grounds = new List<GameObject>();
        }
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<GamePlayEvent>(OnGamePlay);
    }
}
