﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DualDash.Events;

public class PowerUpsManager : Singleton<PowerUpsManager>
{
    [SerializeField] private GameObject m_PiecePowerUpPrefab = null;
    [SerializeField] private Transform m_PieceParent = null;
    private List<GameObject> m_Pieces = new List<GameObject>();


    [SerializeField] private GameObject m_MagnetPowerUpPrefab = null;
    [SerializeField] private GameObject m_ShieldPowerUpPrefab = null;
    [SerializeField] private GameObject m_BoostPowerUpPrefab = null;
    [SerializeField] private Transform m_PowerUpsParent = null;
    private List<GameObject> m_PowerUps = new List<GameObject>();


    [SerializeField] private float m_DistanceSpawnFromPlayer = 55f;
    private Transform m_Player = null;

    private Coroutine m_PieceBorderLeftCoroutine = null;
    private Coroutine m_PieceLeftCoroutine = null;
    private Coroutine m_PieceRightCoroutine = null;
    private Coroutine m_PieceBorderRightCoroutine = null;
    private Coroutine m_PiecesCoroutine = null;
    private Coroutine m_PowerUpsCoroutine = null;

    protected override void Awake()
    {
        base.Awake();
        Player player = FindObjectOfType<Player>();
        if (player != null)
        {
            m_Player = player.transform;
        }
        EventManager.Instance.AddListener<GamePlayEvent>(OnGamePlay);
        EventManager.Instance.AddListener<PlayerGetPowerUpEvent>(OnPlayerGetPowerUp);
        EventManager.Instance.AddListener<DestroyPowerUpEvent>(OnDestroyPowerUp);
    }


    private void OnGamePlay(GamePlayEvent e)
    {
        InitPieces();
        m_PiecesCoroutine = StartCoroutine(SpawnPieces());
        
        InitPowerUps();
        m_PowerUpsCoroutine = StartCoroutine(SpawnPowerUps());
    }

    private void InitPowerUps()
    {
        if (m_PowerUpsCoroutine != null)
        {
            StopCoroutine(m_PowerUpsCoroutine);
            m_PowerUpsCoroutine = null;
        }
        foreach (GameObject powerUp in m_PowerUps)
        {
            Destroy(powerUp);
        }
        m_PowerUps = new List<GameObject>();
    }
    private void InitPieces()
    {
        if (m_PiecesCoroutine != null)
        {
            StopCoroutine(m_PiecesCoroutine);
            m_PiecesCoroutine = null;
        }
        if (m_PieceBorderLeftCoroutine != null)
        {
            StopCoroutine(m_PieceBorderLeftCoroutine);
            m_PieceBorderLeftCoroutine = null;
        }
        if (m_PieceLeftCoroutine != null)
        {
            StopCoroutine(m_PieceLeftCoroutine);
            m_PieceLeftCoroutine = null;
        }
        if (m_PieceRightCoroutine != null)
        {
            StopCoroutine(m_PieceRightCoroutine);
            m_PieceRightCoroutine = null;
        }
        if (m_PieceBorderRightCoroutine != null)
        {
            StopCoroutine(m_PieceBorderRightCoroutine);
            m_PieceBorderRightCoroutine = null;
        }
        foreach (GameObject piece in m_Pieces)
        {
            Destroy(piece);
        }
        m_Pieces = new List<GameObject>();
    }

    private IEnumerator SpawnPowerUps()
    {
        while (true)
        {
            float k = Random.Range(0f, 100f);
            GameObject g;
            if (k < 50)
            {
                g = Instantiate(m_MagnetPowerUpPrefab);
            }
            else if (k < 100)
            {
                g = Instantiate(m_ShieldPowerUpPrefab);
            }
            else
            {
                g = Instantiate(m_BoostPowerUpPrefab);
            }
            g.GetComponent<PowerUp>().PathPosition = PathRandomPosition(0, 3);
            g.transform.position = Vector3.forward * (m_Player.position.z + m_DistanceSpawnFromPlayer + 13f);
            g.transform.SetParent(m_PowerUpsParent);
            m_PowerUps.Add(g);
            yield return new WaitForSeconds(Random.Range(6f, 9f) / (Player.Instance.Speed / Player.Instance.StartSpeed));
        }
    }

    private IEnumerator SpawnPieces()
    {
        while (true)
        {
            if (Random.Range(0f, 100f) < 40f)
            {
                CharacterPosition path = PathRandomPosition(0, 3);
                switch (path)
                {
                    case CharacterPosition.BorderLeft:
                        if (m_PieceBorderLeftCoroutine == null)
                        {
                            m_PieceBorderLeftCoroutine = StartCoroutine(SpawnPiecesLine(path));
                        }
                        break;
                    case CharacterPosition.Left:
                        if (m_PieceLeftCoroutine == null)
                        {
                            m_PieceLeftCoroutine = StartCoroutine(SpawnPiecesLine(path));
                        }
                        break;
                    case CharacterPosition.Right:
                        if (m_PieceLeftCoroutine == null)
                        {
                            m_PieceRightCoroutine = StartCoroutine(SpawnPiecesLine(path));
                        }
                        break;
                    default:
                        if (m_PieceBorderRightCoroutine == null)
                        {
                            m_PieceBorderRightCoroutine = StartCoroutine(SpawnPiecesLine(path));
                        }
                        break;
                }
            }
            yield return new WaitForSeconds(0.5f / (Player.Instance.Speed / Player.Instance.StartSpeed));
        }
    }

    private IEnumerator SpawnPiecesLine(CharacterPosition path)
    {
        for (int i = 0; i < Random.Range(3, 12); i++)
        {
            GameObject g = Instantiate(m_PiecePowerUpPrefab);
            PowerUp powerUpComponent = g.GetComponent<PowerUp>();
            float pieceType = Random.Range(0f, 100f);
            if (pieceType < 8f)
            {
                powerUpComponent.PieceType = PieceValue.Sheep;
            }
            else if (pieceType < 20f)
            {
                powerUpComponent.PieceType = PieceValue.Pig;
            }
            else if (pieceType < 55f)
            {
                powerUpComponent.PieceType = PieceValue.Chicken;
            }
            else
            {
                powerUpComponent.PieceType = PieceValue.Rabbit;
            }
            powerUpComponent.PathPosition = path;
            g.transform.position = Vector3.forward * (m_Player.position.z + m_DistanceSpawnFromPlayer);
            g.transform.SetParent(m_PieceParent);
            m_Pieces.Add(g);
            yield return new WaitForSeconds(0.3f / (Player.Instance.Speed / Player.Instance.StartSpeed));
        }
        switch (path)
        {
            case CharacterPosition.BorderLeft:
                m_PieceBorderLeftCoroutine = null;
                break;
            case CharacterPosition.Left:
                m_PieceLeftCoroutine = null;
                break;
            case CharacterPosition.Right:
                m_PieceRightCoroutine = null;
                break;
            default:
                m_PieceBorderRightCoroutine = null;
                break;
        }
    }

    protected virtual CharacterPosition PathRandomPosition(int minimum, int maximum)
    {
        return (CharacterPosition)(Random.Range(minimum, maximum + 1) * 2 - 3);
    }

    private void OnDestroyPowerUp(DestroyPowerUpEvent e)
    {
        StartCoroutine(DestroyPowerUp(e.powerUp));
    }

    private void OnPlayerGetPowerUp(PlayerGetPowerUpEvent e)
    {
        if (e.powerUpType == PowerUpType.Piece)
        {
            GameManager.Instance.Pieces += (int)e.pieceType * GameManager.Instance.BonusDistance;
            GameManager.Instance.TotalPieces += (int)e.pieceType * GameManager.Instance.BonusDistance;
        }
    }

    private IEnumerator DestroyPowerUp(GameObject powerUp)
    {
        if (powerUp.GetComponent<PowerUp>().Type == PowerUpType.Piece)
        {
            m_Pieces.Remove(powerUp);
            Destroy(powerUp);
        }
        else
        {
            m_PowerUps.Remove(powerUp);
            Destroy(powerUp);
        }
        yield return null;
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveListener<GamePlayEvent>(OnGamePlay);
        EventManager.Instance.RemoveListener<PlayerGetPowerUpEvent>(OnPlayerGetPowerUp);
        EventManager.Instance.RemoveListener<DestroyPowerUpEvent>(OnDestroyPowerUp);
    }
}
