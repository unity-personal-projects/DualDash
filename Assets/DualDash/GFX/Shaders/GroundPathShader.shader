﻿Shader "DualDash/GroundPathShader"
{
    Properties
    {
        _Color ("Color", Color) = (1, 1, 1, 1)
        _MainTex ("Albedo (RGB)", 2D) = "white" { }
        _Glossiness ("Smoothness", Range(0, 1)) = 0.5
        _Metallic ("Metallic", Range(0, 1)) = 0.0
        
        _PathRadius ("Path Radius", Range(0, 2)) = 0.5
        _PathColor ("Path Color", Color) = (1, 0, 0, 1)
        _PathBlendCoef ("Path Blend Coef", Range(0, 1)) = 0.5
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 200
        
        CGPROGRAM
        
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows
        
        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0
        
        sampler2D _MainTex;
        
        struct Input
        {
            float2 uv_MainTex;
            float3 worldPos;
            float3 worldNormal;
        };
        
        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
        
        float _PathRadius;
        fixed4 _PathColor;
        float _PathBlendCoef;
        
        int _NPaths;
        fixed4 _PathsPositions[4];
        fixed4 _PathsDirections[4];
        
        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)
        
        float isPixelInsideAPath(float3 worldPos, float3 worldNormal)
        {
            float nHitPath = 0;
            for (int i = 0; i < _NPaths; i ++)
            {
                float3 direction = _PathsDirections[i];
                worldPos.y = 0;
                _PathsPositions[i].y = 0;
                float distance = length(cross(worldPos - _PathsPositions[i], direction));
                // float dotProduct = dot(worldNormal, direction);
                nHitPath += (1. - step(_PathRadius, distance))/* * (1. - step(0, dotProduct))*/;
            }
            return step(1, nHitPath);
        }
        
        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            
            float pixelIsInsidePath = isPixelInsideAPath(IN.worldPos, IN.worldNormal);
            o.Albedo = lerp(c.rgb, _PathColor, _PathBlendCoef * pixelIsInsidePath);
            
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
        
    }
    FallBack "Diffuse"
}
